#!/usr/bin/env bash

echo "Setting up Egnikai Kubernetes cluster..."
chmod +x "setup.sh"

echo "Cloning egnikai-springback repository..."
git -C egnikai-springback pull || git clone -b k8s-master git@bitbucket.org:siddhag/egnikai-springback.git

echo "Cloning egnikai-engine repository..."
git -C egnikai-engine pull || git  clone -b k8s-master git@bitbucket.org:siddhag/egnikai-engine.git

echo "Copy id_rsa, id_rsa.pub, known_hosts into /egnikai-engine/sshKey."

read -p "Press any key to continue."

echo "Cloning egnikai-candidate-webui repository..."
git -C egnikai-candidate-webui pull || git clone -b k8s-master git@bitbucket.org:siddhag/egnikai-candidate-webui.git

echo "Cloning egnikai-admin-webui repository..."
git -C egnikai-admin-webui pull || git clone -b k8s-master git@bitbucket.org:siddhag/egnikai-admin-webui.git

echo "Stopping Minikube..."
minikube stop

echo "Deleting Minikube..."
minikube delete

echo "Starting Minikube..."
minikube start --memory 6000 --cpus=4 --network-plugin=cni --extra-config=kubelet.network-plugin=cni


echo "installing kubernetes network policy provider cilium..."
kubectl create -n kube-system -f https://raw.githubusercontent.com/cilium/cilium/HEAD/examples/kubernetes/addons/etcd/standalone-etcd.yaml

kubectl create -f https://raw.githubusercontent.com/cilium/cilium/HEAD/examples/kubernetes/1.10/cilium.yaml

echo "installed kubernetes network policy provider cilium..."

echo "Changing to Minikube context..."
eval $(minikube docker-env)

echo "Creating secrets..."
kubectl create secret generic egnikai-secrets --from-literal=db-password=`pass egnikai/dbroot` --from-literal=rmq-password=`pass egnikai/rmq`


kubectl create secret docker-registry docker-credentials \
    --docker-server=https://654872652106.dkr.ecr.us-east-1.amazonaws.com \
    --docker-username=AWS \
    --docker-password=$(<docker.txt) \
    --docker-email=email.com


echo "Building docker images and creating pods..."
cd egnikai-springback
bin/build.sh .
docker build --build-arg EGNIKAI_DB_NAME=egnikaidb -t egnikaidb-img ./db
docker build -t egnikai-app-img .
kubectl create -f db-service.yaml
kubectl create -f springback-service.yaml
kubectl create -f db/db.yaml
cd ..

cd egnikai-engine
bin/build.sh
docker build -t egnikai-engine-image .
kubectl create -f rmq-service.yaml
kubectl create -f rabbitmq.yaml
kubectl create -f engine.yaml
cd ..

cd egnikai-springback
kubectl create -f spring-back.yaml
cd ..

cd egnikai-candidate-webui
bin/build.sh
docker build -t egnikai-web-img .
kubectl create -f candidatewebui-service.yaml
kubectl create -f candidatewebui-nodeport-service.yaml
kubectl create -f candidate-webui.yaml
cd ..

cd egnikai-admin-webui
bin/build.sh
docker build -t egnikai-admin-webui-img .
kubectl create -f admin-webui-service.yaml
kubectl create -f adminwebui-nodeport-service.yaml
kubectl create -f admin-webui.yaml
cd ..

echo "Minikube IP: "
minikube ip

minikube dashboard

