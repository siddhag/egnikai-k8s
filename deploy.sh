#!/usr/bin/env bash

#---- Set the kubectl context before running this script. -----

echo "Please ensure docker.txt is populated with correct password"

cd application_yaml

echo "Creating secrets..."
kubectl create secret generic egnikai-secrets --from-literal=db-password=`pass egnikai/dbroot` --from-literal=rmq-password=`pass egnikai/rmq`

kubectl create secret docker-registry docker-credentials \
    --docker-server=https://654872652106.dkr.ecr.us-east-1.amazonaws.com \
    --docker-username=AWS \
    --docker-password=$(<docker.txt) \
    --docker-email=email.com

echo "Creating Services"
kubectl create -f db-service.yaml
kubectl create -f rmq-service.yaml
kubectl create -f springback-service.yaml
kubectl create -f candidatewebui-service.yaml
kubectl create -f candidatewebui-nodeport-service.yaml
kubectl create -f admin-webui-service.yaml
kubectl create -f adminwebui-nodeport-service.yaml

echo "Creating Replicasets"
kubectl create -f db.yaml
kubectl create -f rabbitmq.yaml
kubectl create -f engine.yaml
kubectl create -f spring-back.yaml
kubectl create -f candidate-webui.yaml
kubectl create -f admin-webui.yaml
