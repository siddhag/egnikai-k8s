# Egnikai-k8s

### Summary
Egnikai is an interview automation tool built in-house at ThoughtWorks!

This repository contains the setup file required to deploy the application on Kubernetes locally.

On running the setup.sh, all the repositories required are cloned, built and deployed.

### Tech Stack

* Kubernetes
* Java 8
* Springboot
* Docker
* MySQL
* RabbitMQ
* Maven
* jGit
* Babel/ES6
* React
* Redux
* Saga middleware
* Bulma framework 
* Sass support

### Additional Tools & Dependencies
* [Pass](https://www.passwordstore.org/)
* VirtualBox
* node

Install minikube using the following command:

```bash
brew install minikube
```

### How to run?
To deploy Egnikai on Kubernetes in the local system, use the following command:

```bash
./initial_setup.sh
```

To get minikube IP, use the following command:

```bash
minikube ip
```

Now you should be able to access candidate UI using {MinikubeIP}:30303

__Note__: To view the status of pods, replicasets and services, open minikube dashboard using the command:
```bash
minikube dashboard
```
